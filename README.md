# UE4GMC14C1 - Natural Language Processing (NLP) and Text Mining (TM)

This Gitlab repository stores all the ressources used during the classes.

- The repository *[documents](https://gricad-gitlab.univ-grenoble-alpes.fr/pinquier/nlp_tutorials/-/tree/main/documents)* contains pieces of lecture, tutorial, exercice, and dataset used in the classs.
- The repository [*code*](https://gricad-gitlab.univ-grenoble-alpes.fr/pinquier/nlp_tutorials/-/tree/main/code) contains the Python code used in the classs.
