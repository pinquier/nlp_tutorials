import uuid

class Keyword():

    def __init__(self, text, lower, lemma, pos):
        self.id = str(uuid.uuid4())
        self.text = text
        self.lower = lower
        self.lemma = lemma
        self.pos = pos