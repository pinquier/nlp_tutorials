# Tutorial 2:
from PyPDF2 import PdfFileReader
import re
import stanza

from nltk.corpus import stopwords
from nltk.corpus import wordnet

# Extract text from a PDF file
def parse_pdf(path, toPrint):

    pdfFileObj = open(path, 'rb') # Create a pdf file object
    pdfReader = PdfFileReader(pdfFileObj) # Create a pdf reader object
    pages = pdfReader.numPages # Get the number of pages in the pdf file
    text_in_pages = [] # Store text for each page of the document

    # Read all the Pages
    for i in range(pages):
        pageObj = pdfReader.getPage(i) # Creating a page object
        text = pageObj.extractText() # Extract text from page
        text_in_pages.append(text) # Add text to the list
        # Write text in a file
        with open('../text.txt', 'a', encoding="utf-8") as f:
            f.write(pageObj.extractText())

        # Print the page number et textual content
        if toPrint:
            print("Page No: ", i)
            print(text)
            print()

    # Close the PDF file object

    return(text_in_pages)

## Load model
def import_model():
    stanza.download('en') # This downloads the English models for the neural pipeline
    stanza_nlp = stanza.Pipeline('en')

# import package for regular expressions with 'import re'
def remove_multiple_whitespace(rawText, toPrint):

    # Remove multiple whitespaces with Regex
    cleanedText = re.sub(' +', ' ', rawText)

    # Print cleaned text
    if toPrint:
        print(cleanedText)
    return cleanedText

# Split raw text into tokens
def tokenization(rawText, toPrint):

    # Define the NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize', tokenize_no_ssplit=True)

    # Create a Stanza document
    doc = nlp(rawText)

    # Store tokens of all sentences
    tokens_of_sentences = []
    # Store tokens of a give sentence
    tokens_of_sentence = []

    # Get tokens
    for sentence in doc.sentences:
        for token in sentence.tokens:
            tokens_of_sentence.append(token)
        tokens_of_sentences.append(tokens_of_sentence)

    # Print tokens
    if toPrint == True:
        for sentence in doc.sentences:
            print(sentence.text)
            for token in sentence.tokens:
                print(token)

    return tokens_of_sentences

# Remove tokens corresponding to stop words
def remove_stop_words(tokens_of_sentences, toPrint):

    # Get list of stop words from NLTK
    stop_words = stopwords.words('english')

    # Filter out tokens corresponding to stop words
    for tokens_of_sentence in tokens_of_sentences:
        for token in tokens_of_sentence:
            if token.text in stop_words:
                tokens_of_sentence.remove(token)

    # Print tokens without stop words
    if toPrint:
        for tokens_of_sentence in tokens_of_sentences:
            for token in tokens_of_sentence:
                print(token)

    return tokens_of_sentences

# Lower case a list of words
def case_folding(words, toPrint):

    # Store list of words in lower case
    lower_cased_words = []

    # Get words in lower case
    for word in words:
        lower_cased_words.append(word.lower())

    # Print words in lower case
    if toPrint:
        for lower_cased_word in lower_cased_words:
            print(word)

    return lower_cased_words

# Split text into sentences
def sentence_splitting(rawText, toPrint):

    # Define the NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize')

    # Create a Stanza document
    doc = nlp(rawText)

    # Get sentences
    sentences = doc.sentences

    # Print Sentences
    if toPrint == True:
        for sentence in doc.sentences:
            print(sentence.text)

    return sentences
