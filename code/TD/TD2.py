# Tutorial 2: Morpho-syntactic analysis

from code.TD.TD1 import *
from code.Domain.Keyword import Keyword

import stanza

from nltk.corpus import stopwords
from nltk.corpus import wordnet

# Do NER
def recognize_named_entities(rawText, toPrint):

    # define NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize,ner')

    # Create a Stanza document
    doc = nlp(rawText)

    # Get named entities
    entities = [sentence.ents for sentence in doc.sentences ]

    # Print named entities
    if toPrint == True:
        for sentence in doc.sentences:
            for ent in sentence.ents:
                print('Word:', ent.text, '->', 'Type:', ent.type)

    return entities

# Tag tokens with POS
def pos_tagging(rawText, toPrint):

    # Define the NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize,mwt,pos')

    # Create a Stanza document
    doc = nlp(rawText)

    # Get tags
    tags = [word.pos for sentence in doc.sentences for word in sentence.words]

    # Print POS tags
    if toPrint == True:
        for sentence in doc.sentences:
            for word in sentence.words:
                print('Word:', word.text, '->', 'POS tag:', word.pos)

    return tags

# Get lemmas of tokens
def lemmatization(rawText, toPrint):

    # Define the NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize,mwt,pos,lemma')

    # Create a Stanza document
    doc = nlp(rawText)

    # Get lemmas
    lemmas = [word.lemma for sentence in doc.sentences for word in sentence.words]

    # Get lemmas
    if toPrint == True:
        for sentence in doc.sentences:
            for word in sentence.words:
                print('Word:', word.text, '->', 'Lemma:', word.lemma)

    return lemmas

# Get dependency graph
def get_dependencies(rawText, toPrint):

    # define NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize,mwt,pos,lemma,depparse')

    # Create a Stanza document
    doc = nlp(rawText)

    # Get dependencies
    for sentence in doc.sentences:
        for word in sentence.words:
            if word.head > 0:
                print('Word:', word.text, '->', 'Head:', sentence.words[word.head - 1].text, '->', 'Type:', word.deprel)
            else:
                print('Word:', word.text, '->', 'Head:', "root", '->', 'Type:', word.deprel)

    # Print named entities
    if toPrint == True:
        for sentence in doc.sentences:
            for word in sentence.words:
                if word.head > 0:
                    print('Word:', word.text, '->', 'Head:', sentence.words[word.head-1].text, '->', 'Type:', word.deprel)
                else:
                    print('Word:', word.text, '->', 'Head:', "root", '->', 'Type:', word.deprel)

                    dependencies=[]
    return dependencies

# Pre-process a chuck of text
def pre_process(rawText, toPrint):

    # List to store keywords for each sentence
    keywords_in_sentences_in_document = []

    # define NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize,ner,mwt,pos,lemma', tokenize_no_ssplit=False)

    # Remove multiple white spaces
    rawText = remove_multiple_whitespace(rawText, False)

    # Create a Stanza document
    doc = nlp(rawText)

    # Pre-process document and stores keyword's features in a Keyword object for each sentence
    for i, sentence in enumerate(doc.sentences):
        #print("[Sentence {}]".format(i + 1))
        keywords_in_sentence = []
        for word in sentence.words:
            # Check if its a noun or an adjective or a verb or an adverb
            if word.pos in ("NOUN", "PROPN", "INTJ", "VERB", "ADJ", "ADV"):
                keyword = Keyword(word.text, word.text.lower(), word.lemma, word.pos)
                keywords_in_sentence.append(keyword)
        keywords_in_sentences_in_document.append(keywords_in_sentence)

    # Print results of the pre-processing
    if toPrint:
        for i, sentence in enumerate(doc.sentences):
            print("[Sentence {}]".format(i + 1))
            for word in sentence.words:
                if word.head > 0:
                    print(
                        "{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:1s}".format( \
                            'Word:', word.text, 'Lower', word.text.lower(), 'Lemma:', word.lemma, 'POS:', word.pos,
                            'Head:', sentence.words[word.head - 1].text, 'Dependency:', word.deprel))
                else:
                    print(
                        "{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:1s}".format( \
                            'Word:', word.text, 'Lower', word.text.lower(), 'Lemma:', word.lemma, 'POS:', word.pos,
                            'Head:', 'root', 'Dependency:', word.deprel))

    return keywords_in_sentences_in_document

# Get sets of synonymes of a word
def get_synonyms(word, toPrint):

    # Get all the sets of synonyms for a given word
    synsets = wordnet.synsets(word)

    # Print the sets of synonyms
    if toPrint:
        for synset in synsets:
            print(synset)
            print(synset.pos)
            print(synset.definition())
            print(synset.examples())
            print(synset.lemmas())
            print('-----------------')
    return synsets

# Get sets of synonymes of a word according its POS
def get_synonyms_by_pos(word, pos, toPrint):

    # Get all the sets of synonyms for a given word according to its POS
    synsets = wordnet.synsets(word, pos)

    # Print the sets of synonyms
    if toPrint:
        for synset in synsets:
            print(synset)
            print(synset.pos)
            print(synset.definition())
            print(synset.examples())
            print(synset.lemmas())
            print('-----------------')
    return synsets

