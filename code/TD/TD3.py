from code.TD.TD2 import *
from wordcloud import WordCloud

import os
import matplotlib.pyplot as plt

# Get all documents in a given folder
def iterate_in_folder(path, toPrint):

    documents = os.listdir(path)

    if toPrint:
        for document in documents:
            print(document)

    return documents

# Get a list of raw texts extracted from all documents stored in a given folder
def parse_documents_in_folder(folder_name):

    texts = []

    documents = iterate_in_folder(folder_name, False)

    for document in documents:
        pages = parse_pdf(folder_name + "/" + document, False)
        text = ' '.join(pages)
        texts.append(text)

    return texts

# Get all keywords per sentences for a given document
def get_keywords_in_sentences_in_document(text):

    keywords_in_sentences_in_document = pre_process(text, False)

    return keywords_in_sentences_in_document

# Get the total number of keywords in a list of documents
def get_nb_keywords(documents):

    nb_keywords_in_total = 0
    nb_keywords_in_document = 0
    nb_keywords_in_documents = []

    for document in documents:
        for sentence in document:
            nb_keywords_in_sentence = len(sentence)
            nb_keywords_in_document = nb_keywords_in_document + nb_keywords_in_sentence
        nb_keywords_in_documents.append(nb_keywords_in_document)

    for nb_keywords_in_document in nb_keywords_in_documents:
        nb_keywords_in_total = nb_keywords_in_total + nb_keywords_in_document

    return nb_keywords_in_total

# Get the average number of keywords per sentences in a list of documents
def get_average_nb_keywords_per_sentences_in_documents(documents):

    average_nb_keywords_per_sentences_in_documents = []

    for document in documents:
        nb_keywords_in_sentences = []
        for sentence in document:
            nb_keywords_in_sentence = len(sentence)
            nb_keywords_in_sentences.append(nb_keywords_in_sentence)
        average_nb_keywords_per_sentences = sum(nb_keywords_in_sentences) / len(nb_keywords_in_sentences)
        average_nb_keywords_per_sentences_in_documents.append(average_nb_keywords_per_sentences)

    return average_nb_keywords_per_sentences_in_documents

# Plot the average number of keywords per sentences in a list of documents
def plot_average_nb_keywords_per_sentences_in_documents(path, avg_nb_kw):

    docNames = iterate_in_folder(path, False)

    plt.bar(docNames, avg_nb_kw, align='center', alpha=0.5)
    plt.title('Average number of keywords per sentence')

    plt.show()

# Plot a cloud of words for a list of raw texts
def plot_wordlcoud(texts):

    full_text = ""

    for text in texts:
        full_text += text

    # Create the wordcloud object
    wordcloud = WordCloud(width=480, height=480, margin=0).generate(full_text)

    # Display the generated image:
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis("off")
    plt.margins(x=0, y=0)
    plt.show()
