from code.TD.TD2 import *
from numpy import dot
from numpy.linalg import norm
from gensim.models import Word2Vec
import gensim.downloader as api

# Get keywords (i.e., nouns, verbs, adverbs, and adjectives) from a chunk of text
def get_keywords(text):

    keywords_in_sentences = pre_process(text,False)

    return keywords_in_sentences

# Build a bag of words from a 2-D list (rows = sentences, columns = keywords)
def build_bow(keywords_in_sentences, toPrint):

    bow = []

    for keywords_in_sentence in keywords_in_sentences:
        for keyword in keywords_in_sentence:
            if keyword.lemma.lower() not in bow:
                bow.append(keyword.lemma.lower())

    if toPrint:
        print("Bag of words: ", bow)

    return bow

# Build a term-document matrix with boolean term occurrence weighting
def build_TD_matrix_boolean(keywords_in_sentences, bow, toPrint):

    matrix = []

    # For each sentence
    for keywords_in_sentence in keywords_in_sentences:
        vector = []
        lemmas = []

        # Get list of lowercased lemmas of the current sentence
        for keyword in keywords_in_sentence:
            lemmas.append(keyword.lemma.lower())

        # Check if a given keyword's lemma of the current sentence is in the bag of words
        for keyword_in_bow in bow:
            if keyword_in_bow in lemmas:
                vector.append(1)
            else:
                vector.append(0)
        matrix.append(vector)

    if toPrint:
        for vector in matrix:
            print(vector)

    return matrix

# Calculate the cosine similarity between two vectors
def calculate_cosine_similartiy(vectorA, vectorB):

    cosine_similarty = dot(vectorA, vectorB)/(norm(vectorA)*norm(vectorB))

    return cosine_similarty

# Calculate the cosine similarity between a vector and entries of a matrix
def compute_similarity(vector, matrix, toPrint):

    scores = []

    for sentence in matrix:
        score = calculate_cosine_similartiy(vector, sentence)
        scores.append(score)

    if toPrint:
        for i, sentence in enumerate(matrix):
            print("The similarity with the sentence", i+1, "has a score of: ", scores[i])

    return scores

def get_word_embedding(word, toPrint):

    similar_words = []

    # (Down)Load a pre-trained Word2Vec model
    corpus = api.load('text8')

    # Load the corps as a Python object
    w2v_model = Word2Vec(corpus)

    # Make sure the Wor2Vec model has been loaded
    print('Successful loading of Word2Vec!')

    # Check if the word is in the pre-trained model
    if word in w2v_model.wv:
        # Get the top 10 most similar words
        similar_words = w2v_model.wv.most_similar(word, topn=10)

    if toPrint:
        print(similar_words)

    return similar_words