import csv
import math

# Read CSV file and load the data in a dictionary
def read_csv_file(path, delimeter_typer):
    with open(path, 'r', encoding="utf8") as text:
        reader = csv.reader(text, delimiter=delimeter_typer)
        dict = {rows[0]: rows[1] for rows in reader}

    return dict

# Compute N1.
def compute_N_10_N11(term, sentences):

    score = 0

    for sentence in sentences:
        lemmas = list(keyword.lemma.lower() for keyword in sentence)
        if term in lemmas:
            score += 1

    return score

# Compute N0.
def compute_N_01_N00(term, sentences):

    score = 0

    for sentence in sentences:
        lemmas = list(keyword.lemma.lower() for keyword in sentence)
        if term not in lemmas:
            score += 1

    return score

# Compute the mutual informations score
def compute_MI( N_10, N_11, N_01, N_00, N_1X, N_0X, N_X1, N_X0, N):
    MI = 0
    try:
        MI = N_11 / N * math.log2((N * N_11) / (N_1X * N_X1)) + N_01 / N * math.log2((N * N_01) / (N_0X * N_1X)) + N_10 / N * math.log2((N * N_10) / (N_1X * N_0X)) + N_00 / N * math.log2((N * N_00) / (N_0X * N_X0))
    except (ZeroDivisionError, ValueError):
        pass

    return MI

# Get list of top X terms with the highest mutual information score
def get_topX(keywords_MI, columnToRank, topX):
    ranking = sorted(keywords_MI, key=lambda x: x[columnToRank], reverse=True)[:topX]

    return ranking
