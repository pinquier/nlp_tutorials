import stanza
import re
from sklearn import svm
import random

# Shuffle rows of a dictionary
def shuffle_dictionary(dic):

    l = list(dic.items())
    random.shuffle(l)
    dic = dict(l)
    return dic

# Tokenization a chunk of text to return a 2D list (rows = sentences, columns = tokens)
def get_tokens(sentence):

    tokens_of_sentence = []

    # Define the NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize,mwt,pos,lemma')

    # Create a Stanza document
    doc = nlp(sentence)

    for sentence in doc.sentences:
        tokens_of_sentence.append(sentence.words)

    # Get tokens
    return tokens_of_sentence

# Check if the sentence contains a prescriptive term
def contains_prescriptive(sentence):

    prescriptive_terms = ["specified", "as specified in", "as specified at", "as specified under", "as detailed in", "detailed in", "as defined in", "defined in", "in accordance with", "in accordance to",
                          "prescribed in," "as set out in", "specified herein", "comply with", "in agreement with", "i.a.w", "be compatible with", "conform to", "stated in", "refer to"]

    for prescriptive_term in prescriptive_terms:
        if prescriptive_term in sentence:
            return True

# Check if the sentence contains a term that is in the list of standardisation organisation accronyms
def contains_standards(sentence):

    prescriptive_terms = ["ECSS", "ISO", "IEEE", "CS", "IEC", "ARP"]

    for prescriptive_term in prescriptive_terms:
        if prescriptive_term in sentence:
            return True

# Check if the sentence contains elements of a document structure
def contains_structure(sentence):

    prescriptive_terms = ["$", "section", "chapter", "page"]

    for prescriptive_term in prescriptive_terms:
        if prescriptive_term in sentence:
            return True

# Check if the sentence contains a term that has multiple capitalised characters
def contains_mulitple_upercases(sentence):

    pattern = "[A-Z][A-Z]+"
    return re.search(pattern, sentence)

# Check if the sentence contains a term that is a blend of capitalised characters and digits
def contains_mix_of_uppercases_digits(sentence):
    pattern = "([A-Za-z]+[\d@]+[\w@]*|[\d@]+[A-Za-z]+[\w@]*)"
    return re.search(pattern, sentence)

# Check if brackets in a sentence
def contains_brackets(sentence):
    brackets = ["(", "{", "[", "]", "}", ")"]

    for bracket in brackets:
        if bracket in sentence:
            return True

# Check in a token is a preposition
def contains_preposition(tokens_of_sentences):

    for tokens_of_sentence in tokens_of_sentences:
        for token in tokens_of_sentence:
            if token.pos == "ADP":
                return True

# Check if a lemma is in the list of terms corresponding to legal documents
def contains_legal(tokens_of_sentences):

    legals = ["standard", "regulation", "guideline", "code_1", "procedure", "law", "norm"]
    for tokens_of_sentence in tokens_of_sentences:
        for token in tokens_of_sentence:
            if token.lemma in legals:
                return True

# Check if a lemma contains multiple dash
def contains_multiple_dash(tokens_of_sentences):

    for tokens_of_sentence in tokens_of_sentences:
        for token in tokens_of_sentence:
            return token.lemma.count("-") >= 2

# Check if a lemma contains multiple dots
def contains_multiple_dots(tokens_of_sentences):

    pattern = "^[0-9]*\.[0-9]+([eE][0-9]+)?$"

    for tokens_of_sentence in tokens_of_sentences:
        for token in tokens_of_sentence:
            if re.search(pattern, token.lemma):
                return True

# Transform a list of sentences in a set of features vectors
def get_features(sentences):

    features = []

    for sentence in sentences:

        features_vector = []

        if contains_prescriptive(sentence):
            features_vector.append("1")
        else:
            features_vector.append("0")

        if contains_mulitple_upercases(sentence):
            features_vector.append("1")
        else:
            features_vector.append("0")

        if contains_brackets(sentence):
            features_vector.append("1")
        else:
            features_vector.append("0")

        if contains_mix_of_uppercases_digits(sentence):
            features_vector.append("1")
        else:
            features_vector.append("0")

        if contains_standards(sentence):
            features_vector.append("1")
        else:
            features_vector.append("0")

        if contains_structure:
            features_vector.append("1")
        else:
            features_vector.append("0")

        tokens = get_tokens(sentence)

        if contains_preposition(tokens):
            features_vector.append("1")
        else:
            features_vector.append("0")

        if contains_multiple_dots(tokens):
            features_vector.append("1")
        else:
            features_vector.append("0")

        if contains_multiple_dash(tokens):
            features_vector.append("1")
        else:
            features_vector.append("0")

        if contains_legal(tokens):
            features_vector.append("1")
        else:
            features_vector.append("0")


        features.append(features_vector)

    return features

# Write features vectors in txt file
def write_feature(features, labels):
    # Read all the Pages
    for i, feature_vector in enumerate(features):
        # Write text in a file
        line = ' '.join(feature_vector)+","+labels[i]+"\n"
        with open('Datasets/Exercice_Classification/features.txt','w',encoding="utf-8") as f:
            f.write(line)

# Train a machine learning algorithm and classify the testing set
def classify(features_training, labels_training, features_testing):

    # Select SVM as machine learning algorithm
    clf = svm.SVC()
    # Train an SVM machine learning algorithm
    clf.fit(features_training, labels_training)
    # Apply the classified on the testing set
    testing_labels = clf.predict(features_testing)
    # Return the predicted labels
    return testing_labels