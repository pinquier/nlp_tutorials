from code.TD.TD3 import *
from code.TD.TD4 import *
from code.TD.TD5 import *
from code.TD.TD6 import *

def run_tuto_1():

    # Path to folder that contains the documents
    path_folder = "Datasets/NLP/"
    # Get a list of raw text that corresponds to the parsing of each document
    texts_in_documents = parse_documents_in_folder(path_folder)

    # Store the keywords of each sentence in a document
    keywords_in_documents = []

    # For each document
    for text_in_document in texts_in_documents:
        # Get the keywords of each sentence in a document
        keywords_in_sentences = get_keywords_in_sentences_in_document(text_in_document)
        # 3-dimension [document, sentence, keywords] list that stores the keywords of every sentences in all documents
        keywords_in_documents.append(keywords_in_sentences)

    # Get total number of keywords after cleaning
    total_nb_keywords = get_nb_keywords(keywords_in_documents)
    print("Total number of keywords", total_nb_keywords)

    # Get average number of keywords per sentence in each document
    average_nb_keywords_per_sentences_in_documents = get_average_nb_keywords_per_sentences_in_documents(keywords_in_documents)
    print(average_nb_keywords_per_sentences_in_documents)

    # Plot average number of keywords per sentence in each document
    plot_average_nb_keywords_per_sentences_in_documents(path_folder, average_nb_keywords_per_sentences_in_documents)

    # Plot a cloud of words for all documents
    plot_wordlcoud(texts_in_documents)

def run_tuto_3():

    text = "Grenoble is a small city with a high number of students. There are many students in engineering in the city of Grenoble. Grenoble has also students in social science."

    keywords_in_sentences = get_keywords(text)
    keywords_in_sentences = remove_stop_words(keywords_in_sentences, False)

    bow = build_bow(keywords_in_sentences, False)

    TD_matrix = build_TD_matrix_boolean(keywords_in_sentences, bow, False)

    mySentence = "There are many engineering students in Grenoble for such a small city."
    keywords_in_mySentence = get_keywords(mySentence)

    TD_vector = build_TD_matrix_boolean(keywords_in_mySentence, bow, False)
    compute_similarity(TD_vector, TD_matrix, True)

    #get_word_embedding("Grenoble", True)

def run_tuto_4():

    dataset = read_csv_file("Datasets/Tuto_features_selection/Sentences_SME.csv", ",")

    sentences = list(dataset.keys())
    labels = list(dataset.values())

    sentences_ME = []
    sentences_EE = []
    sentences_RAMS = []
    sentences_CS = []

    for i, sentence in enumerate(sentences):
        if labels[i] == 'ME':
            sentences_ME.append(sentence)
        elif labels[i] == 'EE':
            sentences_EE.append(sentence)
        elif labels[i] == 'RAMS':
            sentences_RAMS.append(sentence)
        else:
            sentences_CS.append(sentence)

    text = ' '.join(sentences)

    text_ME = ' '.join(sentences_ME)
    text_EE = ' '.join(sentences_EE)
    text_RAMS = ' '.join(sentences_RAMS)
    text_CS = ' '.join(sentences_CS)

    keywords_in_sentences = get_keywords(text)

    keywords_in_sentences = remove_stop_words(keywords_in_sentences, False)
    initial_bow = build_bow(keywords_in_sentences, False)

    keywords_in_sentences_ME = get_keywords(text_ME)
    keywords_in_sentences_EE = get_keywords(text_EE)
    keywords_in_sentences_RAMS = get_keywords(text_RAMS)
    keywords_in_sentences_CS = get_keywords(text_CS)

    keywords_MI = []

    for keyword in initial_bow:

        keyword_MI = []

        N_10_ME = compute_N_10_N11(keyword, keywords_in_sentences_EE + keywords_in_sentences_RAMS + keywords_in_sentences_CS)
        N_10_EE = compute_N_10_N11(keyword, keywords_in_sentences_ME + keywords_in_sentences_RAMS + keywords_in_sentences_CS)
        N_10_RAMS = compute_N_10_N11(keyword, keywords_in_sentences_ME + keywords_in_sentences_EE + keywords_in_sentences_CS)
        N_10_CS = compute_N_10_N11(keyword, keywords_in_sentences_ME + keywords_in_sentences_EE + keywords_in_sentences_RAMS)

        N_11_ME = compute_N_10_N11(keyword, keywords_in_sentences_ME)
        N_11_EE = compute_N_10_N11(keyword, keywords_in_sentences_EE)
        N_11_RAMS = compute_N_10_N11(keyword, keywords_in_sentences_RAMS)
        N_11_CS = compute_N_10_N11(keyword, keywords_in_sentences_CS)

        N_01_ME = compute_N_01_N00(keyword, keywords_in_sentences_ME)
        N_01_EE = compute_N_01_N00(keyword, keywords_in_sentences_EE)
        N_01_RAMS = compute_N_01_N00(keyword, keywords_in_sentences_RAMS)
        N_01_CS = compute_N_01_N00(keyword, keywords_in_sentences_CS)

        N_00_ME = compute_N_01_N00(keyword, keywords_in_sentences_EE + keywords_in_sentences_RAMS + keywords_in_sentences_CS)
        N_00_EE = compute_N_01_N00(keyword, keywords_in_sentences_ME + keywords_in_sentences_RAMS + keywords_in_sentences_CS)
        N_00_RAMS = compute_N_01_N00(keyword, keywords_in_sentences_ME + keywords_in_sentences_EE + keywords_in_sentences_CS)
        N_00_CS = compute_N_01_N00(keyword, keywords_in_sentences_ME + keywords_in_sentences_EE + keywords_in_sentences_RAMS)

        N_1X_ME = N_10_ME + N_11_ME
        N_1X_EE = N_10_EE + N_11_EE
        N_1X_RAMS = N_10_RAMS + N_11_RAMS
        N_1X_CS = N_10_CS + N_11_CS

        N_0X_ME = N_00_ME + N_01_ME
        N_0X_EE = N_00_EE + N_01_EE
        N_0X_RAMS = N_00_RAMS + N_01_RAMS
        N_0X_CS = N_00_CS + N_01_CS

        N_X1_ME = N_01_ME + N_11_ME
        N_X1_EE = N_01_EE + N_11_EE
        N_X1_RAMS = N_01_RAMS + N_11_RAMS
        N_X1_CS = N_01_CS + N_11_CS

        N_X0_ME = N_00_ME + N_10_ME
        N_X0_EE = N_00_EE + N_10_EE
        N_X0_RAMS = N_00_RAMS + N_10_RAMS
        N_X0_CS = N_00_CS + N_10_CS

        N = len(sentences)

        MI_ME = compute_MI(N_10_ME, N_11_ME, N_01_ME, N_00_ME, N_1X_ME, N_0X_ME, N_X1_ME, N_X0_ME, N)
        MI_EE = compute_MI(N_10_EE, N_11_EE, N_01_EE, N_00_EE, N_1X_EE, N_0X_EE, N_X1_EE, N_X0_EE, N)
        MI_RAMS = compute_MI(N_10_RAMS, N_11_RAMS, N_01_RAMS, N_00_RAMS, N_1X_RAMS, N_0X_RAMS, N_X1_RAMS, N_X0_RAMS, N)
        MI_CS = compute_MI(N_10_CS, N_11_CS, N_01_CS, N_00_CS, N_1X_CS, N_0X_CS, N_X1_CS, N_X0_CS, N)

        keyword_MI.append(keyword)
        keyword_MI.append(MI_ME)
        keyword_MI.append(MI_EE)
        keyword_MI.append(MI_RAMS)
        keyword_MI.append(MI_CS)

        keywords_MI.append(keyword_MI)

    print(keywords_MI)

    print("ME:", get_topX(keywords_MI,1,20))
    print("EE:", get_topX(keywords_MI,2,20))
    print("RAMS:", get_topX(keywords_MI,3,20))
    print("MCS:", get_topX(keywords_MI,4,20))

def run_tuto_5():
    dataset = read_csv_file("Datasets/Exercice_Classification/dataset.csv", ";")

    dataset = shuffle_dictionary(dataset)

    sentences = list(dataset.keys())
    labels = list(dataset.values())

    features = get_features(sentences)

    training_sentences = sentences[:-20]
    training_features = features[:-20]
    training_labels = labels[:-20]

    testing_sentences = sentences[-20:]
    testing_features = features[-20:]
    testing_labels = labels[-20:]

    write_feature(features,labels)
    print("features writen")

    predicted_testing_labels = classify(training_features, training_labels, testing_features)

    for i, testing_sentence in enumerate(testing_sentences):
        print(testing_sentence, "predicted:", predicted_testing_labels[i], "actual:", testing_labels[i])

if __name__ == '__main__':

    run_tuto_5()






