# UE4GMC14C1 - NLP&TM - #1. NLP Pipeline - Tuto 1

This tutorial 1 on NLP pipeline concentrates on the **pre-processing**, which is a phase of a text mining process that consists in cleaning raw documents so as to normalize the vocabulary before to convert it to computable vectors.



**Again, again and again, there is no unique pre-processing pipeline, its definition depends on the use case and requires testing verious alternatives!!!**

## 1. Parsing

Before pre-processing, it's often necessary to extract the text from a document. We call this operation ***parsing.*** Documents can be classified into 3 categories: structured, semi-structured and unstructured. **Structured documents** are databases that can be queried with a formal language. **Unstructured documents** are texts writen in natural language such as MS Word file. **Semi-structured documents** such as MS Excel are more structured than unstructured ones but not enough to be queried with a formal language. In this tutorial, we give an example for parsing a PDF unstructured document with the [PyPDF2](https://pythonhosted.org/PyPDF2/) package.

Download the [Python PyPDF2](https://pypi.org/project/PyPDF2/) package by typing the instruction ***pip install pypdf2*** in the terminal.

Import the [Python PyPDF2](https://pypi.org/project/PyPDF2/) package with the instruction:

```
from PyPDF2 import PdfFileReader
```

Extract text from the [PDF files](https://gricad-gitlab.univ-grenoble-alpes.fr/pinquier/nlp_tutorials/-/tree/main/code/Datasets/NLP) with the function:

```python
# Extract text from a PDF file
def parse_pdf(path, toPrint):

    pdfFileObj = open(path, 'rb') # Create a pdf file object
    pdfReader = PdfFileReader(pdfFileObj) # Create a pdf reader object
    pages = pdfReader.numPages # Get the number of pages in the pdf file
    text_in_pages = [] # Store text for each page of the document

    # Read all the Pages
    for i in range(pages):
        pageObj = pdfReader.getPage(i) # Creating a page object
        text = pageObj.extractText() # Extract text from page
        text_in_pages.append(text) # Add text to the list
        # Write text in a file
        with open('text.txt', 'a', encoding="utf-8") as f:
            f.write(pageObj.extractText())

        # Print the page number et textual content
        if toPrint:
            print("Page No: ", i)
            print(text + '\n\n')
            print()

    # Close the PDF file object
    pdfFileObj.close()

    return(text_in_pages)
```

**To extract text from a MS Word** document you can use the package [docx2python](https://docx2python.readthedocs.io/en/latest/) or [python-docx](https://python-docx.readthedocs.io/en/latest/) or [docx2text](https://github.com/ankushshah89/python-docx2txt) (all of them have a Python wrapper). The package docx2python is of particular interest as it enables you to convert the MS Word document into a semi-structured HTML file. Working with a HTML file facilitates the extraction of the content since it contains HTML tags to retrieve elements of structure, such as headings, lists, enumerations, footnotes, etc.the instruction. To convert a PDF into HTML with docx2python, use the instruction: `doc_html_result = docx2python('FILE_PATH', html=True)`.

When the **PDF is a scan**, you can use an Optical Character Recognition (OCR) engine such as [Google's Tesseract](https://github.com/tesseract-ocr/tesseract), especially its [Python wrapper](https://pypi.org/project/pytesseract/).

## 2. Multiple white spaces removing

While writing a document, the author may type multiple white spaces. It also occurs when you parse unstructured files. It's always good to start removing them with the **regular expression**: *re.sub(' +', ' ', YOUR_TEXT)*. Don't forget to **import the package 're'** that will enable you to use regular expressions in Python.  Regular expressions (regex) is a powerfull tool for NLP. Learn more about regex [here](https://www.w3schools.com/python/python_regex.asp) and test your regex [here](https://regex101.com/).

```python
# import package for regular expressions with 'import re'
def remove_multiple_whitespace(rawText, toPrint):

    # Remove multiple white spaces with Regex
    cleanedText = re.sub(' +', ' ', rawText)

    # Print cleaned text
    if toPrint:
        print(cleanedText)
    return cleanedText
```

## 3. Case folding

“The” and “the” will be treated as two different unigrams if no case-folding. Thus, a common strategy to normalise words into the same form is to do *case-folding* by ***reducing all letters to lower case***. Often this is a good idea: it will allow instances of *Automobile* at the beginning of a sentence to match with a query of *automobile*. It will also help on a web search engine when most of your users type in *ferrari* when they are interested in a *Ferrari* car. Below an example to lower case a list of words with the Python function ***lower()***.

```python
# Lower case a list of words
def case_folding(words, toPrint):

    # Store list of words in lower case
    lower_cased_words = []

    # Get words in lower case
    for word in words:
        lower_cased_words.append(word.lower())

    # Print words in lower case
    if toPrint:
        for lower_cased_word in lower_cased_words:
            print(lower_cased_word)

    return lower_cased_words
```

**Such case folding can equate words that might better be kept apart**. Many proper nouns are derived from common nouns and so are distinguished only by case, including companies (*General Motors*, *The Associated Press*), government organisations (*the Fed* vs. *fed*) and person names (*Bush*, *Black*). For English, an alternative to making every token lowercase is to just make some tokens lowercase. **The simplest heuristic is to convert to lowercase words at the beginning of a sentence and all words occurring in a title that is all uppercase or in which most or all words are capitalised**. These words are usually ordinary words that have been capitalised. **Mid-sentence capitalised words are left as capitalised** (which is usually correct). This will mostly avoid case-folding in cases where distinctions should be kept apart. The same task can be done more accurately by a machine learning sequence model which uses more features to make the decision of when to case-fold. This is known as ***truecasing*** . However, trying to get capitalisation right in this way probably doesn't help if your users usually use lowercase regardless of the correct case of words. Thus, **lowercasing everything often remains the most practical solution**.

## 4. Tokenization

The first step in handling text is *to **break the stream of characters into words or, more precisely, tokens***. This is fundamental to further analysis. Without identifying the tokens, it is difficult to imagine extracting higher-level information from the document. Each token is an instance of a type, so the number of tokens is much higher than the number of types. As an example, in the previous sentence there are two tokens spelled “the.” These are both instances of a type “the,” which occurs twice in the sentence. Properly speaking, one should always refer to the frequency of occurrence of a type, but loose usage also talks about the frequency of a token. Breaking a stream of characters into tokens is trivial for a person familiar with the language structure. A computer program, though, being linguistically challenged, would find the task more complicated. The reason is that certain characters are sometimes
token delimiters and sometimes not, depending on the application. The characters space, tab, and newline we assume are always delimiters and are not counted as tokens. They are often collectively called white space. The characters ( ) < > ! ? " are always delimiters and may also be tokens. The characters . , : - ’ may or may not be delimiters, depending on their environment. A period, comma, or colon between numbers would not normally be considered a delimiter but rather part of the number. Any other comma or colon is a delimiter and may be a token. A period can be part of an abbreviation (e.g., if it has a capital letter on both sides). It can also be part of an abbreviation when followed by a space (e.g., Dr.). However, some of these are really ends of sentences. The problem of detecting when a period is an end of sentence and when it is not will be discussed later. For the purposes of tokenization, it is probably best to treat any ambiguous period as a word delimiter and also as a token.

Below is an example of the tokenization of a piece of text with Stanza. If you print the list of tokens, then you should notice that a token is an Object. See the definition of the Object [here](https://stanfordnlp.github.io/stanza/data_objects.html#token). If you want to print the property *text* of the Object, then use the instruction *print(token.text)*.

Download the [Stanza](https://pypi.org/project/PyPDF2/) package by typing the instruction ***pip install stanza*** in the terminal.

Import the [Stanza](https://stanfordnlp.github.io/stanza/) package with the instruction:

```
import stanza
```

Tokenize a chunk of text with the function:

```python
# Split raw text into tokens
def tokenization(rawText, toPrint):

    # Define the NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize', tokenize_no_ssplit=True)

    # Create a Stanza document
    doc = nlp(rawText)

    # Store tokens of all sentences
    tokens_of_sentences = []
    # Store tokens of a give sentence
    tokens_of_sentence = []

    # Get tokens
    for sentence in doc.sentences:
        for token in sentence.tokens:
            tokens_of_sentence.append(token)
        tokens_of_sentences.append(tokens_of_sentence)

    # Print tokens
    if toPrint == True:
        for sentence in doc.sentences:
            print(sentence.text)
            for token in sentence.tokens:
                print(token)

    return tokens_of_sentences
```

## 5. Remove stop words

Sometimes, some extremely **common words that appear to be of little value are excluded from the vocabular**y entirely. These words are called ***stop words***. The general strategy for determining a stop list is to sort the terms by ***collection frequency*** (the total number of times each term appears in the document collection), and then to take the most frequent terms, often hand-filtered for their semantic content relative to the domain, as a ***stop list***, the members of which are then discarded. Here we use the stop list of the NLTK package that you can import with the instruction 'from nltk.corpus import stopwords'. In general, to avoid impacting the tokenization task, it's better to filter out stop words from the list of tokens.

Download the [NLTK](https://www.nltk.org/) package by typing the instruction ***pip install nltk*** in the terminal.

Import the [NLTK](https://www.nltk.org/) package with the instruction:

```
from nltk.corpus import stopwords
```

The first time you must download the list of stop words. Thus, type the instruction ***nltk.download()*** the same way you import a package and run the code. A download window will pop up and you should select the ***Corpora > stopwords***. Once you have imported the list of stop words, you can comment the line ***nltk.download()***.

Remove remove stop words with the function:

```python
# Remove tokens corresponding to stop words
def remove_stop_words(tokens_of_sentences, toPrint):

    # Get list of stop words from NLTK
    stop_words = stopwords.words('english')

    # Filter out tokens corresponding to stop words
    for tokens_of_sentence in tokens_of_sentences:
        for token in tokens_of_sentence:
            if token.text in stop_words:
                tokens_of_sentence.remove(token)

    # Print tokens without stop words
    if toPrint:
        for tokens_of_sentence in tokens_of_sentences:
            for token in tokens_of_sentence:
                print(token)

    return tokens_of_sentences
```

## 6. Sentence splitting

Sentence segmentation consists in **splitting a chunck of text into sentences**. At first glance, it might look an easy task that consists in implementing a **regex** such as `"^\\s+[A-Za-z,;'\"\\s]+[.?!]$"`:

-  `^` means "begins with"
-  `\\s` means white space
-  `+` means 1 or more
-  `[A-Za-z,;'"\\s]` means any letter, `,`, `;`, `'`, `"`, or whitespace character
-  `$` means "ends with"

However, with such a rule, you may get some unexpected results.  You should better use [Stanza sentence segmentation](https://stanfordnlp.github.io/stanza/tokenize.html) with the function:

```python
# Split a chunck of text into sentences
def sentence_splitting(rawText, toPrint):

    # Define the NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize')

    # Create a Stanza document
    doc = nlp(rawText)

    # Get sentences
    sentences = doc.sentences

    # Print Sentences
    if toPrint:
        for sentence in doc.sentences:
            print(sentence.text)

    return sentences
```

See examples of sentence splitting models implemented in the package CoreNLP (i.e. Stanza equivalent in Java) [here](https://stanfordnlp.github.io/CoreNLP/ssplit.html).

Note that s**entence splitting requires tokenization** but that t**okenization does not require sentence splitting**.
