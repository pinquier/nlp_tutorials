# UE4GMC14C1 - NLP&TM - #2. NLP Pipeline - Tuto 2

This tutorial 2 on NLP pipeline concentrates on the **morpho-syntactic analysis**, which is phase of a text mining process that consists in analysing the word structure (morphology) and the grammar of sentences (syntax). To experience most pre-processing tasks introduced hereunder, you can use the web-based [CoreNLP application](https://corenlp.run/).



**Again, again and again, there is no unique pre-processing pipeline, its definition depends on the use case and requires testing verious alternatives!!!**

## 1. Recognise named entities

The named entity recognition (NER) module recognizes mention spans of a particular entity type (e.g., Person or Organization) in the input sentence. 

Make sure you have downloaded and imported the Stanza package (see Step #4).

```python
# Tag named entities
def recognize_named_entities(rawText, toPrint):

    # Define NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize,ner')

    # Create a Stanza document
    doc = nlp(rawText)

    # Get named entities
    entities = [sentence.ents for sentence in doc.sentences]

    # Print named entities
    if toPrint:
        for sentence in doc.sentences:
            for ent in sentence.ents:
                print('Word:', ent.text, '->', 'Type:', ent.type)

    return entities
```

## 2. Part-Of-Speech Tagging

Part-of-speech tagging (tagging for short) is **the process of assigning a part-ofspeech marker to each word in an input text**. Because tags are generally also applied to punctuation, **tokenization is usually performed before, or as part of, the tagging process**: separating commas, quotation marks, etc., from words and disambiguating end-of-sentence punctuation (period, question mark, etc.) from part-of-word punctuation (such as in abbreviations like e.g. and etc.). The **input** to a tagging algorithm is a **sequence of words** and a **tagset**, and the **output** is a **sequence of tags**, a single best tag for each word as shown in the example:

- INPUT: A New York City firm

- OUTPUT: a/DT New/NNP York/NNP City/NNP firm/NN

Tagging is a disambiguation task; words are ambiguous —have more than one possible part-of-speech— and the goal is to find the correct tag for the situation. For example, the word *book* can be a verb (*<u>book</u> that flight*) or a noun as in (*hand me that <u>book</u>*).

To tag tokens with their POS, you can use the [Stanza POS and morphological features](https://stanfordnlp.github.io/stanza/pos.html) with the function:

```python
# Tag each taken with a Part-Of-Speech
def pos_tagging(rawText, toPrint):

    # Define the NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize,mwt,pos')

    # Create a Stanza document
    doc = nlp(rawText)

    # Get tags
    tags = [word.pos for sentence in doc.sentences for word in sentence.words]

    # Print POS tags
    if toPrint:
        for sentence in doc.sentences:
            for word in sentence.words:
                print('Word:', word.text, '->', 'POS tag:', word.pos)

    return tags
```

## 3. Lemmatization (and Stemming)

Lemmatization usually refers to doing things properly with the use of a vocabulary and **morphological analysis** of words, normally aiming **to remove inflectional endings only and to return the base or dictionary form of a word**, which is known as the **lemma**. If confronted with the token *saw*, **stemming** might return just ***s***, whereas **lemmatization would attempt to return either *see* or *saw* depending on whether the use of the token was as a verb or a noun**. The two may also differ in that **stemming most commonly collapses derivationally related word**s, whereas **lemmatization commonly only collapses the different inflectional forms of a lemma**. While stemming and lemmatization **help a lot for some cases, it equally hurts performance a lot for others**. Stemming increases **recall** while harming **precision**. As an example of what can go wrong, note that the Porter stemmer stems all of the following words: *operate operating operates operation operative operatives operational* to *oper*. However, since operate in its various forms is a common verb, we would expect to lose considerable precision on queries such as the following with Porter stemming:

- operational AND research
- operating AND system
- operative AND dentistry

For a case like this, moving to using a lemmatizer would not completely fix the problem because particular inflectional forms are used in particular collocations: a sentence with the words *operate* and *system* is not a good match for the query *operating AND system*.

To lemmatize the tokens, you can use the [Stanza lemmatizer](https://stanfordnlp.github.io/stanza/lemma.html) with the function: 

```python
# Get lemmas of tokens
def lemmatization(rawText, toPrint):

    # Define the NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize,mwt,pos,lemma')

    # Create a Stanza document
    doc = nlp(rawText)

    # Get lemmas
    lemmas = [word.lemma for sentence in doc.sentences for word in sentence.words]

    # Print lemmas
    if toPrint:
        for sentence in doc.sentences:
            for word in sentence.words:
                print('Word:', word.text, '->', 'Lemma:', word.lemma)

    return lemmas
```

Remember you won't get the same results with all packages (Stanza, NLTK, Spacy, etc.) since they don't implement the same algorithms. You could try the tokenization of NLTK and observe differences with Stanza.

## 4. Dependencies

For a chunk of text, you can get a **semantic graph** thanks to the [Stanza dependency parsing](https://stanfordnlp.github.io/stanza/depparse.html) capability. The dependency parsing module builds a tree structure of words from the input chunk of text, which represents the syntactic dependency relations between words. Give a try to the web-based [CoreNLP application](https://corenlp.run/) to get illustrations of the dependecy parsing.

```python
# Get dependencies
def get_dependencies(rawText, toPrint):

    # define NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize,mwt,pos,lemma,depparse')

    # Create a Stanza document
    doc = nlp(rawText)

    # Get dependencies
    for sentence in doc.sentences:
        for word in sentence.words:
            if word.head > 0:
                print('Word:', word.text, '->', 'Head:', sentence.words[word.head - 1].text, '->', 'Type:', word.deprel)
            else:
                print('Word:', word.text, '->', 'Head:', "root", '->', 'Type:', word.deprel)

    # Print named entities
    if toPrint == True:
        for sentence in doc.sentences:
            for word in sentence.words:
                if word.head > 0:
                    print('Word:', word.text, '->', 'Head:', sentence.words[word.head-1].text, '->', 'Type:', word.deprel)
                else:
                    print('Word:', word.text, '->', 'Head:', "root", '->', 'Type:', word.deprel)

                    dependencies=[]
    return dependencies
```

## 5. Pre-processing pipeline

In practice, the pre-processing task consists in defining a pipeline that combines all functions. Below is an example of a pre-processing pipeline.

```python
# Pre-process a chunk of text
def pre_process(rawText, toPrint):

    # List to store keywords for each sentence
    keywords_in_sentences_in_document = []

    # Define NLP pipeline
    nlp = stanza.Pipeline(lang='en', processors='tokenize,ner,mwt,pos,lemma', tokenize_no_ssplit=False)

    # Remove multiple white spaces
    rawText = remove_multiple_whitespace(rawText, False)

    # Create a Stanza document
    doc = nlp(rawText)

    # Pre-process document and stores keyword's features in a Keyword object for each sentence
    for i, sentence in enumerate(doc.sentences):
        #print("[Sentence {}]".format(i + 1))
        keywords_in_sentence = []
        for word in sentence.words:
            # Check if its a noun or an adjective or a verb or an adverb
            if word.pos in ("NOUN", "PROPN", "INTJ", "VERB", "ADJ", "ADV"):
                keyword = Keyword(word.text, word.text.lower(), word.lemma, word.pos)
                keywords_in_sentence.append(keyword)
        keywords_in_sentences_in_document.append(keywords_in_sentence)

    # Print results of the pre-processing
    if toPrint:
        for i, sentence in enumerate(doc.sentences):
            print("[Sentence {}]".format(i + 1))
            for word in sentence.words:
                if word.head > 0:
                    print(
                        "{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:1s}".format( \
                            'Word:', word.text, 'Lower', word.text.lower(), 'Lemma:', word.lemma, 'POS:', word.pos,
                            'Head:', sentence.words[word.head - 1].text, 'Dependency:', word.deprel))
                else:
                    print(
                        "{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:15s}\t{:1s}\t{:1s}".format( \
                            'Word:', word.text, 'Lower', word.text.lower(), 'Lemma:', word.lemma, 'POS:', word.pos,
                            'Head:', 'root', 'Dependency:', word.deprel))

    return keywords_in_sentences_in_document
```

define an **object** to store the features of a keyword (e.g. raw text, raw text in lower cases, lemma, POS tag,) so as to facilitate their reuse for future tasks.

```python
import uuid

class Keyword():

    def __init__(self, text, lower, lemma, pos):
        self.id = str(uuid.uuid4())
        self.text = text
        self.lower = lower
        self.lemma = lemma
        self.pos = pos
```

## 6. Get syonyms with WordNet

In your NLP or TM applications, you will often need to get synonyms, definitions of terms, or related terms. For instance, when a term has several meanings. One popular domain knowledge resource to collect such linguistic features is the [thesaurus WordNet](https://wordnet.princeton.edu/). You can give it a try via the [web-based application](http://wordnetweb.princeton.edu/perl/webwn).

You can access to the WordNet thesaurus from Python, as you did before (see Step #5) to dowload the list of stop words. Thus, type the instruction ***nltk.download()*** the same way you import a package and run the code. A download window will pop up and you should select the ***Corpora > wordnet*** or ***wordnet31***. Once you have imported the list of stop words, you can comment the line ***nltk.download()***.

To access to the WordNet package from Python, you can import 'wordnet' from the 'nltk.corpus'.

```
from nltk.corpus import wordnet
```

Get the sets of synonyms (aka. synsets) of a given word from WordNet with the function:

```python
# Get sets of synonymes of a word
def get_synonyms(word, toPrint):

    # Get all the sets of synonyms for a given word
    synsets = wordnet.synsets(word)

    # Print the sets of synonyms
    if toPrint:
        for synset in synsets:
            print(synset)
            print(synset.pos)
            print(synset.definition())
            print(synset.examples())
            print(synset.lemmas())
            print('-----------------')
    return synsets
```

Note that the synsets depend on the POS tag (grammatical category) of the token (e.g. book) and the meaning (e.g. bank). You can get the synsets of a word for a given POS with the function:

```python
# Get sets of synonymes of a word according its POS
def get_synonyms_by_pos(word, pos, toPrint):

    # Get all the sets of synonyms for a given word according to its POS
    synsets = wordnet.synsets(word, pos)

    # Print the sets of synonyms
    if toPrint:
        for synset in synsets:
            print(synset)
            print(synset.pos)
            print(synset.definition())
            print(synset.examples())
            print(synset.lemmas())
            print('-----------------')
    return synsets
```

## 7. Get related concepts with ConceptNet

WordNet is a thesaurus that concentrates on linguistic relationships. If you want to get related concepts that belongs to common knowledge, then you should better use the ontology [ConceptNet 5](https://conceptnet.io/). To run queries on ConceptNet, you can build the ontology locally with the [ConceptNet](https://pypi.org/project/ConceptNet/) Python package (need Unix command line, Python 3.5 or later, 30GB of RAM, and some other dependencies) or you can use the [REST API](https://github.com/commonsense/conceptnet5/wiki/API). If needed, I encourage you tu use the REST API for this course.

Write a function to collect the related concepts of a given word via the ConceptNet REST API.
