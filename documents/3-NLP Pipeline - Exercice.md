# UE4GMC14C1 - NLP&TM - #3. NLP Pipeline - Exo

This exercice aims at developing functions that implement the main text pre-processing tasks introduced in the tutorias [NLP Pipeline 1](https://gricad-gitlab.univ-grenoble-alpes.fr/pinquier/nlp_tutorials/-/blob/main/documents/1-NLP%20pipeline%20-%20TD%201.md) and [NLP Pipeline 2](https://gricad-gitlab.univ-grenoble-alpes.fr/pinquier/nlp_tutorials/-/blob/main/documents/2-NLP%20pipeline%20-%20TD%202.md). In addition to the pre-processing, you will create some basic visuals of descriptive statistics learnt in a previous course.

## 1. Text extraction and pre-processing

Write the functions required to extract the text from three PDF documents and pre-process the text to store the keywords - i.e., adjectives, adverbs, nouns, and verbs - of each sentence contained in each document.

You may use the functions below to get the list of documents in a given folder and extract the text of each document.

```python
# Get all documents in a folder
def iterate_in_folder(path, toPrint):

    documents = os.listdir(path)

    if toPrint:
        for document in documents:
            print(document)

    return documents

# Get the textual content of docuements in a folder
def parse_documents_in_folder(folder_name):

    texts = []

    documents = iterate_in_folder(folder_name, False)

    for document in documents:
        pages = parse_pdf(folder_name + "/" + document, False)
        text = ' '.join(pages)
        texts.append(text)

    return texts
```

## 2. Text analytics

- Calculate the total number of keywords before cleaning (feel free to implement any cleaning strategy)
- Calculate the total number of keywords after cleaning (feel free to implement any cleaning strategy)
- Calculate the average number of tokens per sentences after cleaning for each document
- With the package [Matplotlib](https://www.tutorialspoint.com/matplotlib/matplotlib_bar_plot.htm), for each document, plot the average number of tokens per sentences after cleaning

![image-20211126123051246](../images/image-20211126123051246.png)

- With the package [Wordcloud](https://pypi.org/project/wordcloud/) based on this [example](https://www.python-graph-gallery.com/wordcloud/), plot a cloudword of the dataset keywords collected in Section 1.

![image-20211126123158266](../images/image-20211126123158266.png)
