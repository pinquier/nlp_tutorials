

# UE4GMC14C1 - NLP&TM - #4. Text Representation - Tuto

This tutorial 3 on Text Reprensentation contrates on the transformation of raw text into a computable format, that is, to find a scheme to represent it mathematically as we do it for images.

![image-20211126124853247](../images/image-20211126124853247.png)

Let's take three sentences:

- (S1) "Grenoble is a small city with a high number of students."
- (S2) "There are many students in engineering in the city Grenoble."
- (S3) "Grenoble has also students in social science."

**How would you reprent these sentences in a computable format?!**

## 1. Vector Space Model

The **Vector Space Model (VSM)** is a simple **algebraic model** that is extensively used for representing any text in many information-retrieval operations, from scoring documents on a query to document classification and clustering. VSM is **a mathematical model that represents text units as vectors**.

![image-20211126132811597](../images/image-20211126132811597.png)

## 2. Bag of Words 

The **Bag of Words (BoW)** representation is a **vector representation** that **does not consider the ordering of words** in a document. Thus, the sentences *"John is quicker than Mary"* and *"Mary is quicker than John"* have the same vectors. **Each document is treated as a vector** named a bag of words where **each word/term becomes a feature**. The set of bag of words forms what we call a **Term-Document matrix**.

|                           | john | quicker | mary |
| :-----------------------: | :--: | :-----: | :--: |
| John is quicker than Mary |      |         |      |
| Mary is quicker than John |      |         |      |

- [ ] What's the bag of words?
- [ ] What's the size of this Vector Space?
- [ ] Build a bag of words for the chunk of text "*Grenoble is a small city with a high number of students. There are many students in engineering in the city of Grenoble. Grenoble has also students in social science*."

```python
# Get keywords (i.e., nouns, verbs, adverbs, and adjectivs) from a chunk of text
def get_keywords(text):

    keywords_in_sentences = pre_process(text,False)

    return keywords_in_sentences

# Build a bag of words from a 2-D list (rows = sentences, columns = keywords)
def build_bow(keywords_in_sentences, toPrint):

    bow = []

    for keywords_in_sentence in keywords_in_sentences:
        for keyword in keywords_in_sentence:
            if keyword.lemma.lower() not in bow:
                bow.append(keyword.lemma.lower())

    if toPrint:
        print("Bag of words: ", bow)

    return bow

if __name__ == '__main__':

    text = "Grenoble is a small city with a high number of students. There are many students in engineering in the city of Grenoble. Grenoble has also students in social science."

    # Get a list of keywords (i.e., nouns, verbs, adverbs, and adjectivs)
    keywords = get_keywords(text)
    
	# Remove stop words (e.g. the verb BE) from the list of keywords
    keywords = remove_stop_words(keywords,False)
    
    # Build a bag of words
    bow = build_bow(keywords)
```

## 3. Boolean Term Occurence

The simplest technique for vector creation is to use boolean values to represent the occurence or not of a term in a document.

- (S1) "Grenoble is a small city with a high number of students."
- (S2) "There are many students in engineering in the city Grenoble."
- (S3) "Grenoble has also students in social science."

|        | **grenoble** | **small** | **city** | **high** | **number** | **student** | **many** | **engineering** | also | **social** | **science** |
| :----: | :----------: | :-------: | :------: | :------: | :--------: | :---------: | :------: | :-------------: | :--: | :--------: | :---------: |
| **S1** |      1       |     1     |    1     |    1     |     1      |      1      |    0     |        0        |  0   |     0      |      0      |
| **S2** |      1       |     0     |    1     |    0     |     0      |      1      |    1     |        1        |  0   |     0      |      0      |
| **S3** |      1       |     0     |    0     |    0     |     0      |      1      |    0     |        0        |  1   |     1      |      1      |

Raw term frequency is sometimes not what we want. Indeed, a document with 10 occurrences of the term "*INP*" is more relevant than a document with 1 occurrence of the term "*INP*". However, it is not 10 times more relevant since **relevance does not increase proportionally with term frequency**.

- [ ] *Remove stopwords and build a Term-Document matrix using Boolean weighting where documents correspond to the sentences 1, 2 and 3.*

```python
# Build a term-document matrix with boolean term occurence weighting
def build_TD_matrix_boolean(keywords_in_sentences, bow, toPrint):

    matrix = []

    # For each sentence
    for keywords_in_sentence in keywords_in_sentences:
        vector = []
        lemmas = []

        # Get list of lowercased lemmas of the current sentence
        for keyword in keywords_in_sentence:
            lemmas.append(keyword.lemma.lower())

        # Check if a given keyword's lemma of the current sentence is in the bag of words
        for keyword_in_bow in bow:
            if keyword_in_bow in lemmas:
                vector.append(1)
            else:
                vector.append(0)
        matrix.append(vector)

    if toPrint:
        for vector in matrix:
            print(vector)

    return matrix
```

## 4. Calculate similarity

Given the sentence *S0* "*There are many engineering students in Grenoble for such a small city.*".

- (S1) "Grenoble is a small city with a high number of students."
- (S2) "There are many students in engineering in the city Grenoble."
- (S3) "Grenoble has also students in social science."

|          | **grenoble** | **small** | **city** | **high** | **number** | **student** | **many** | **engineering** | also | **social** | **science** |
| :------: | :----------: | :-------: | :------: | :------: | :--------: | :---------: | :------: | :-------------: | :--: | :--------: | :---------: |
| ***S0*** |      1       |     1     |    1     |    0     |     0      |      1      |    1     |        1        |  0   |     0      |      0      |

Given the Term-Document matrix as follows.

|        | **grenoble** | **small** | **city** | **high** | **number** | **student** | **many** | **engineering** | also | **social** | **science** |
| :----: | :----------: | :-------: | :------: | :------: | :--------: | :---------: | :------: | :-------------: | :--: | :--------: | :---------: |
| **S1** |    **1**     |   **1**   |  **1**   |    1     |     1      |    **1**    |    0     |        0        |  0   |     0      |      0      |
| **S2** |    **1**     |     0     |  **1**   |    0     |     0      |    **1**    |  **1**   |      **1**      |  0   |     0      |      0      |
| **S3** |      1       |     0     |    0     |    0     |     0      |      1      |    0     |        0        |  1   |     1      |      1      |

- [ ] Which sentence among *S1*, *S2*, and *S3* is the most similar to *S0*?
- [ ] Calculate the cosine similarity between (*S0*, *S1*), (*S0*, *S2*), and (*S0*, *S3*). Which sentence is the most similar to *S0*? What's the corresponding similartiy score?

![Cosine Similarity - Neo4j Graph Data Science](../images/cosine-similarity.png)

```python
from numpy import dot
from numpy.linalg import norm

# Calculate the cosine similarity between two vectors
def calculate_cosine_similartiy(vectorA, vectorB):

    cosine_similarty = dot(vectorA, vectorB)/(norm(vectorA)*norm(vectorB))

    return cosine_similarty

# Calculate the cosine similarity between a vector and entries of a matrix
def compute_similarity(vector, matrix, mySentence, toPrint):

    scores = []

    for sentence in matrix:
        score = calculate_cosine_similartiy(vector, sentence)
        scores.append(score)

    if toPrint:
        for i, sentence in enumerate(matrix):
            print("The similarity with the sentence", i+1, "has a score of: ", scores[i])

    return scores
```

## 5. Term Frequency

The **term frequency** tf*t,d* of term *t* in document *d* is defined as the **number of times that *t* occurs in *d***. Since **different documents** may be of different lengths, a term may occur more often in a longer document as compared to a shorter document. To **normalize** these counts, we divide the number of occurrences by the length of the document.

![image-20211126154803945](../images/image-20211126154803945.png)


## 6. Document Frequency

Nevertheless, rare terms are more informative than frequent terms. Indeed, consider a term in the query that is rare in the collection (e.g., *xylophone*). A document containing this term is very likely to be relevant to the query *xylophone*. Thus, we want a high weight for rare terms like *xylophone*. In addition, frequent terms are less informative than rare terms. Indeed, consider a query term that is frequent in the collection (e.g., *high, increase, line*). A document containing such a term is more likely to be relevant than a document that doesn’t, but it’s not a sure indicator of relevance. Consequently, for frequent terms, we want high positive weights for words like *high, increase, and line*, but lower weights than for rare terms. We use document frequency (df) to capture this.

The document frequency df*t* is the document frequency of *t*, that is, the **number of documents that contain *t***.

## 7. Term Frequency(TF)-Inverse Document Frequency (IDF)

TF-IDF is the best known weighting scheme in information retrieval as it increases with the number of occurrences within a document and increases with the rarity of the term in the collection. The TF-IDF score is a product of TF and IDF.

![image-20211126153549451](../images/image-20211126153549451.png)

![image-20211126153609223](../images/image-20211126153609223.png)

## 8. Word embedding

If we look back at all the representation schemes that we’ve discussed so far, we notice three fundamental drawbacks:

- They’re discrete representations—i.e., they treat language units (words, n-grams, etc.) as atomic units. This discreteness hampers their ability to capture relationships between words.
- The feature vectors are sparse and high-dimensional representations. The dimensionality increases with the size of the vocabulary, with most values being zero for any vector. This hampers learning capability. Further, high-dimensionality representation makes them computationally inefficient.
- They cannot handle OOV words.

These fundamental drawbacks motivated the development of new text representations, especially **word embedding**. For the set of words in a corpus, ***embedding*** is a mapping between vector space coming from **distributional representation** to vector space coming from **distributed representation**.

What does it mean when we say a text representation should capture “distributional similarities between words”? Let’s consider some examples. If we’re given the word “USA,” distributionally similar words could be other countries (e.g., Canada, Germany, India, etc.) or cities in the USA. If we’re given the word “beautiful,” words that share some relationship with this word (e.g., synonyms, antonyms) could be considered distributionally similar words. These are words that are likely to occur in similar contexts. In 2013, a seminal work by Mikolov *et al.* showed that their neural network–based word representation model known as **Word2vec**, based on “distributional similarity,” can capture word analogy relationships such as:

```
King – Man + Woman ≈ Queen
```

While learning such semantically rich relationships, **Word2vec** ensures that the learned word representations are low dimensional (vectors of dimensions 50–500, instead of several thousands) and dense (that is, most values in these vectors are non-zero). Conceptually, Word2vec takes a large corpus of text as input and “learns” to represent the words in a common vector space based on the contexts in which they appear in the corpus.

**Training** your own word embeddings **is a pretty expensive process** (in terms of both time and computing). Thankfully, for many scenarios, it’s not necessary to train your own embeddings, and using **pre-trained word embeddings** often suffices. What are pre-trained word embeddings? Someone has done the hard work of training word embeddings on a **large corpus**, such as Wikipedia, news articles, or even the entire web, and has put words and their corresponding vectors on the web. These embeddings can be downloaded and used to get the vectors for the words you want. Such embeddings can be thought of as a large collection of key-value pairs, where keys are the words in the vocabulary and values are their corresponding word vectors. Some of the most popular pre-trained embeddings are **Word2vec** by Google, **GloVe** by Stanford, and fasttext **embeddings** by Facebook. In the example below, we use the library Gensim  with a pre-trained Word2Vec model to get the 10 most similar words of a query word. Gensim also supports training and loading GloVe pre-trained models.

```python
from gensim.models import Word2Vec
import gensim.downloader as api

def get_word_embedding(word, toPrint):

    similar_words = []

    # (Down)Load a pre-trained Word2Vec model
    corpus = api.load('text8')

    # Load the corps as a Python object
    w2v_model = Word2Vec(corpus)

    # Make sure the Wor2Vec model has been loaded
    print('Successful loading of Word2Vec!')

    # Look for the 10 most similar words
    if word in w2v_model.wv:
        similar_words = w2v_model.wv.most_similar(word, topn=10)

    if toPrint:
        print(similar_words)

    return similar_words
```

- most_similar(word) returns the most similar words to the word. Each word is accompanied by a similarity score. The higher the score, the more similar the word is to the query word:

- w2v_model returns the vector for the query word.

- [ ] By reusing the data of Section 4, use Word2vec to extend the list of keywords with semantically related terms before computing the cosine similartiy between sentences like in Section 4. What do you observe regarding the similarity scores compared to the bag of words model?
