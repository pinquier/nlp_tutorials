# UE4GMC14C1 - NLP&TM - #5. Features Engineering - Tuto

Remember the **bag of words** text representation model...

- (S1) "Grenoble is a small city with a high number of students."
- (S2) "There are many students in engineering in the city Grenoble."
- (S3) "Grenoble has also students in social science."

|        | **grenoble** | **small** | **city** | **high** | **number** | **student** | **many** | **engineering** | also | **social** | **science** |
| :----: | :----------: | :-------: | :------: | :------: | :--------: | :---------: | :------: | :-------------: | :--: | :--------: | :---------: |
| **S1** |      1       |     1     |    1     |    1     |     1      |      1      |    0     |        0        |  0   |     0      |      0      |
| **S2** |      1       |     0     |    1     |    0     |     0      |      1      |    1     |        1        |  0   |     0      |      0      |
| **S3** |      1       |     0     |    0     |    0     |     0      |      1      |    0     |        0        |  1   |     1      |      1      |

## Use Case: Machine Learning-based Text Classification

**Supervised statistical learning** (in mathematics) or **machine learning** (in computer science) involves building a statistical model for **predicting**, or estimating, **an output based on one or more inputs**. The behaviour of the machine **doesn't need to be explicitly programmed** as the **machine learns to perform a task** or a sequence of tasks **from a massive set of examples**.

![image-20211129162936464](../images/image-20211129162936464.png)

In theory, a machine learns how to classify by **estimating an unknown function *f*** such as ***y = f(x) + ∈*** or ***y = f(x<sub>1</sub>,x<sub>2</sub>,...,x<sub>n</sub>) + ∈***

In practice, the problem is to **estimate the unknown function *f* linking *X* and *y***, that is, to find a fuction:

<img src="../images/image-20211130155027345.png" alt="image-20211130155027345" style="zoom: 67%;" />

## Features

In a **Term-Document matrix**, each column stands as a **feature**. Given the potentially large number of **words**, **sentences**, **typographical** elements, and **layout** artifacts that even a short document may have – not to mention the potentially vast number of different **senses** that each of these elements may have in various contexts and combinations – an essential task for most text mining systems is the **identification of a simplified subset of document features that can be used to represent a particular document as a whole**. We refer to such a set of features as the ***representational* model** of a document and say that **individual documents are *represented by* the *set of features*** that their representational models contain.

However, even with attempts to develop efficient representational models, each document in a collection is usually made up of a **large number – sometimes an exceedingly large number – of features**. Problems relating to high feature dimensionality are typically of much greater magnitude in **text mining** systems than in classic data mining systems. For even the most modest document collections, the **number of word-level features** required to represent the documents in these collections can be **exceedingly large**. For example, in an extremely small collection of 15,000 documents culled from Reuters news feeds, more than 25,000 nontrivial word stems could be identified. Even when one works with more optimized feature types, tens of thousands of concept-level features may still be relevant for a single application domain. The number of attributes in a relational database that are analyzed in a data mining task is usually significantly smaller.

Another characteristic of natural language documents is what might be described as ***feature sparsity**.* Only a small percentage of all possible features for a document collection as a whole appears in any single document, and thus **when a document is represented** as a binary vector of features, **nearly all values of the vector are zero**.

Because **text mining algorithms operate on the feature-based representations** of documents and not the underlying documents themselves, there is often a **tradeoff** between two important goals:

- **Accuracy**: The accuracy of many learning algorithms can be improved by **selecting the most predictive features**. For example, Naïve Bayes tends to perform poorly without feature selection in text classification settings. The purpose of feature selection is sometimes described as a need to eliminate useless noise words, but a study showed that even the lower ranked words continue to have predictive value - only a small set of words are truly equally likely to occur in each class. Thus, **feature selection** may be viewed as selecting those words with **the strongest signal-to-noise ratio**. Pragmatically, the goal is **to select whatever subset of features yields a highly accurate classifier**.

- **Scalability**: A large text corpus can easily have **tens to hundreds of thousands** of distinct words. By **selecting only a fraction of the vocabulary as input**, the induction algorithm may require a great deal **less computation**. This may also yield **savings in storage or network bandwidth**. These benefits could be an enabling factor in some applications, e.g. involving large numbers of classifiers to train.

  

## Features Selection

**The number of different words (features ;) ) is large even in relatively small documents** such as short news articles or paper abstracts. The number of different words in big document collections can be huge. The **dimension of the bag-of-words feature space for a big collection can reach hundreds of thousands**; moreover, the document representation vectors, although **sparse**, may still **have hundreds and thousands of nonzero components.**

**Most of those words are irrelevant** to the categorization task and can be **dropped with no harm to the performanc**e and **may even result in improvement owing to noise reduction**. The preprocessing step that removes the irrelevant words is called feature selection. Most text mining systems at least remove the **stop words** – the function words and in general the common words of the language that usually do not contribute to the semantics of the documents and have **no real added value**. Many systems, however, perform a **much more aggressive filtering, removing 90 to 99 percent of all features**.

<img src="../images/image-20211130142525168.png" alt="image-20211130142525168" style="zoom: 80%;" />

There are three major paradigms of feature selection:

- **Filter methods**: The overall feature selection procedure is to **score each feature according to a particular feature selection metric**, and determine a ranking of all features, from which the best ***k* features are selected**.  Scoring involves counting the occurrences of a feature associated to categories in the training examples, and then computing a function of these. First, **rare words may be eliminated**, on the grounds that they are **unlikely to be present to aid in future classifications**. For example, words occurring two or fewer times may be removed. Easily half of the total number of distinct words may occur only a single time, so **eliminating words under a given low rate of occurrence yields great savings**. The particular choice of **threshold value can have an effect on accuracy**. Additionally, overly common words, such as ‘a’ and ‘of’, may also be removed on the grounds that they occur so frequently as to not be discriminating for any particular class. Observe that the **non-stopwords** ‘computer’ and ‘algorithm’ **may behave like stopwords** in a computer science dataset. This illustrates that **stopwords are not only language-specific, but also domain-specific**. It is also to be mentioned that the common practice of **stemming** or **lemmatizing** - **merging various word forms such as plurals and verb conjugations into one distinct term** - also **reduces the number of features** to be considered.

- **Wrapper methods**: The wrapper methods consist in using wrapper to wrap the learning algorithm during the phase of features selection. They use classic AI search methods - such as greedy hill-climbing or simulated-annealing - to search for the ‘best’ subset of features, **repeatedly evaluating different feature subsets** via cross-validation with a particular induction algorithm. In other words, for a given learning algorithm (e.g. SVM), the algorithm wil be trained as many times there are combinations of features. The search of the different features combinations consists in finding the subset of features that gives the best classification performances. Typically, the space is searched greedily:

  - You start with **no attributes and add them one at a time** - this is called **forward selection**. 
  - You start with the **full set of attributes and delete one at a time** - this is **backward elimination**.
  - You start with an initial subset of attributes (e.g. half) and add or delete one at a time according to the improvement or not of the performances - this is called bidirectional 

  Wrapper methods enables to **remove** **irrelevant** as well as **redundant**. However, for **large scale problems**, wrapper methods are often **impractical**, and **instead, feature scoring metrics (filter methods) are used independently on each feature**.

- **Embedded methods** build a usually linear prediction model that simultaneously tries to maximize the goodness-of-fit of the model and minimize the number of input features. Some variants build a classifier on the full dataset, and then iteratively remove features the classifier depends on least. By beginning with the full dataset, they qualify as the least scalable. Given large feature spaces, memory may be exceeded simply to realize the full feature vectors with all potential features. We will not consider such methods further. Filter methods are the simplest to implement and the most scalable. Hence, they are appropriate to treat very large feature spaces and are the focus here. They can also be used as a pre-processing step to reduce the feature dimensionality sufficiently to enable other, less scalable methods. Wrapper methods have traditionally sought specific combinations of individual features from the power set of features, but this approach scales poorly for the large number of features inherent with classifying text. Using cross-validation to select among feature generators and optimize other parameters is somewhat like a wrapper method, but one that involves far fewer runs of the induction algorithm than typical wrapper feature selection.

An ancillary feature engineering choice is the **representation of the feature value**. Often a **Boolean** indicator of whether the word occurred in the document is sufficient. Other possibilities include the **count of the number of times the word occurred in the document** (TF), the **frequency of its occurrence normalized by the length of the document** (TF/nb word in D), the **count normalized by the inverse document frequency of the word** (TF-IDF). In situations where the document length varies widely, it may be important to normalize the counts.  Further, in **short documents, words are unlikely to repeat, making Boolean word indicators nearly as informative as counts**. This yields **great savings in training resources** and in the **search space** of the induction algorithm. In the following sections, we will study some techniques for representing the feature value for filter methods.

## Frequency (Binary, TF, TF-IDF...)

In order to perform the filtering, a **measure of the relevance** of each feature needs to be defined. Probably the simplest such measure is the **document frequency DF(w)**. Experimental evidence suggests that **using only the top 10 percent of the most frequent words does not reduce the performance of classifiers**. This seems to contradict the well-known Zipf-like distribution, according to which the **terms with low-to-medium document frequency are the most informative**. There is no contradiction, however, because the **large majority of all words have a very low document frequency**, and the **top 10 percent do contain all low-to-medium frequency words**.

**More sophisticated measures of feature relevance** exist that take into account the **relations between features and the categories**.

## Mutual Information

Mutual Information (aka. Information Gain) measures **how much information** - in the information-theoretic sense - a term contains about the category. In other words, it measures **how much information the presence/absence of a feature *f* contributes to making the correct classification** decision on category *c*. MI reaches its **maximum value** if the term is a perfect indicator for class membership, that is, **if the term is present in a document if and only if the document is in the class**.

<img src="../images/image-20211129145845921.png" alt="image-20211129145845921" style="zoom:80%;" />



- *e<sub>t</sub>* = 1 if the document contains the feature *t*
- *e<sub>t</sub>* = 0 if the document doesn't contain the feature *t*
- *e<sub>c</sub>* = 1 if the document belongs to category *c*
- *e<sub>c</sub>* = 0 if the document doesn't belong to category *c*

The previous equation for measuring Mutual Information can be rewritten by replacing the terms *P(U=* e<sub>t</sub> , C=*e<sub>c</sub>*) such that:

<img src="../images/image-20211130152753616.png" alt="image-20211130152753616" style="zoom:67%;" />

Giving the arithmetical form:

<img src="../images/image-20211130152832784.png" alt="image-20211130152832784" style="zoom:67%;" />

With *N*<sub>e<sub>t</sub>e<sub>c</sub></sub> such that:

- With *N*<sub>10</sub> : the number of documents containing the term *t* (*e<sub>t</sub> = 1*) and don't belong to the category *C (e<sub>c</sub> = 0*)
- With *N*<sub>11</sub> : the number of documents containing the term *t* (*e<sub>t</sub> = 1*) and belong to the category *C (e<sub>c</sub> = 1*)
- With *N*<sub>01</sub> : the number of documents not containing the term *t* (e<sub>t</sub> = 0) and that belong to the category *C (e<sub>c</sub> = 1*)
- With *N*<sub>00</sub> : the number of documents not  containing the term *t* (*e<sub>t</sub> = 0*) and that don't belong to the category *C (e<sub>c</sub> = 0*)
- With *N*<sub>1.</sub> = *N*<sub>10</sub> + *N*<sub>11</sub>: the number of documents containing the term *t* (*e<sub>t</sub> = 1*) independently of the category *C (e<sub>c</sub> ∈ {0,1})*
- With *N*<sub>0.</sub> = *N*<sub>00</sub> + *N*<sub>01</sub>: the number of documents not containing the term *t* (e<sub>t</sub> = 0) independently of category *C (e<sub>c</sub> ∈ {0,1})*
- With *N*<sub>.1</sub> = *N*<sub>01</sub> + *N*<sub>11</sub>: the number of documents belonging to the category *C (e<sub>c</sub> = 1)*
- With *N*<sub>.0</sub> = *N*<sub>00</sub> + *N*<sub>10</sub>: the number of documents not belonging to the category *C (e<sub>c</sub> = 0)*
- With *N* : *N*<sub>10</sub> + *N*<sub>11</sub> + *N*<sub>01</sub> + *N*<sub>10</sub> the total number of documents

Thus, given a training set, if we consider the category *mechanics* and the term *voltage*, the calculation of the combinations gives:

|                                               | *e<sub>c</sub> = e<sub>mechanics</sub> = 1* | *e<sub>c</sub> = e<sub>mechanics</sub> = 0* |
| --------------------------------------------- | ------------------------------------------- | ------------------------------------------- |
| ***e<sub>t</sub> = e<sub>voltage</sub> = 1*** | *N*<sub>11</sub> = 49                       | *N*<sub>10</sub> = 27652                    |
| ***e<sub>t</sub> = e<sub>voltage</sub> = 0*** | *N*<sub>01</sub> = 141                      | *N*<sub>01</sub> = 774106                   |

Remember that the mutual information between two random variables is **a non-negative value**, which measures the **dependency between the variables**. It is **equal to zero** if and only if **two random variables are independent**, and **higher values mean higher dependency**.

<img src="../images/image-20211130162728155.png" alt="image-20211130162728155" style="zoom: 50%;" />

Therefore, the Mutual Information *I(U;C)* of the term *voltage* for the category *mechanics* is equal to 0.0001105, which means that both are relatively independent. In other words, the term *voltage* is not a relevant feature for the category *mechanics*.

<img src="../images/image-20211130162801434.png" alt="image-20211130162801434" style="zoom: 50%;" />

- [ ] For the sentences in the [CSV file](https://gricad-gitlab.univ-grenoble-alpes.fr/pinquier/nlp_tutorials/-/tree/main/code/Datasets/Tuto_features_selection), get the keywords (i.e. nouns, verbs, adjectives, adverbs)
- [ ] For each category and each keyword, compute the utility using the Mutual Information *I(U;C)* measure
- [ ] For each category, sort the keywords from the largest to the lowest value
- [ ] Select the most relevant keywords, that is, the *K* (to be empirically defined) terms with the largest mutual information value.

## Chi-Squared (*X<sup>2</sup>*)

Chi square measures the maximal strength of dependence between the feature and the categories. In statistics, the *X<sup>2</sup>* test is applied to test the independence of two events, where two events A and B are defined to be *independent* if *P(AB) = P(A)P(B)* or, equivalently, *P(A|B) = P(A)* and *P(B|A)=P(B)*. In feature selection, the two events are the occurrence of the term and the occurrence of the class. *X<sup>2</sup>* is a measure of how much expected counts (*E*) and observed counts (*N*) deviate from each other. 

Le test du *X<sup>2</sup>* se calcule de la manière suivante :

<img src="../images/image-20211129145919433.png" alt="image-20211129145919433" style="zoom:80%;" />

Like Mutual Information,  *X<sup>2</sup>*  as an arithmetical form:

![image-20211130145407287](../images/image-20211130145407287.png)

If you want to compute the Chi-squared measure of features, see an example [here](https://nlp.stanford.edu/IR-book/html/htmledition/feature-selectionchi2-feature-selection-1.html).

In feature selection, we aim **to select the features which are highly dependent on the response**. When two features are independent, the observed count is close to the expected count, thus we will have a smaller Chi-Square value. So a **high Chi-Square value indicates that the hypothesis of independence is incorrect**. In simple words, the **higher the Chi-Square value is,  the more dependent is the feature to the category, and it can be selected for model training.**

Experiments show that both measures (and several other measures) can **reduce the dimensionality by a factor of 100 without loss of classification quality** – or even with a **small improvement**.

## Features Extraction

Another way of reducing the number of dimensions is **to create a new, much smaller set of synthetic features from the original feature set**. In effect, this amounts to creating **a transformation from the original feature space to another space of much lower dimension**. The rationale for using synthetic features rather than naturally occurring words (as the simpler feature filtering method does) is that, **owing to polysemy, homonymy, and synonymy, the words may not be the optimal features**. By transforming the set of features it may be possible to create document representations that do not suffer from the problems inherent in those properties of natural language. For instance, **term clustering addresses the problem of synonymy by grouping together words with a high degree of semantic relatedness**. These word groups are then used as features instead of individual words.

<img src="../images/image-20211130162958905.png" alt="image-20211130162958905" style="zoom:50%;" />

## References

1. Witten, I. H. (2011) Data mining: practical machine learning tools and techniques.
2. Forman, G. (2007) Feature selection for text classification. Chapter in *Computational Methods of Feature Selection.*
3. Forman, G. (2003) An extensive empirical study of feature selection metrics for text classification. *Journal of Machinea Learning Research.*
4. Manning, C. D. (2008) Introduction to information retrieval. *Cambridge University Press* (online version: https://nlp.stanford.edu/IR-book/)
