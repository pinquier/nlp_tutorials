# UE4GMC14C1 NLP&TM: 6. Classification *VS.* Clustering

This course is a theoretical and practical introduction to the fundamental concepts of machine learning-based text classification and clustering.

## Machine Learning

Remember that machine learning involves building a **statistical mode**l for **predicting**, or **estimating**, **an output based on one or more inputs**. The behaviour of the machine **doesn't need to be explicitly programmed** as the **machine learns to perform a task** or a sequence of tasks **from a massive set of examples**.



![image-20211203122558959](../images/image-20211203122558959.png)

**Statistical learning** (in mathematics) or **machine learning** (in computer science) problems fall into one of two categories:

- **supervised** - For each observation of the predictor measurement(s) *X* there is an associated response measurement *y*. We wish to fit a model that relates the response to the predictors, with the aim of accurately predicting the response for future observations (prediction) or better understanding the relationship between the response and the predictors (inference).

![image-20211129162936464](../images/image-20211129162936464.png)

- **Unsupervised**. - It describes the somewhat more challenging situation in which for every observation *i = 1, . . . , n,* we observe a vector of measurements *x<sub>i</sub>* but no associated response *y<sub>i</sub>*. It is not possible to fit a linear regression model, since there is no response variable to predict. In this setting, we are in some sense working blind; the situation is referred to as unsupervised because we lack a response variable that can supervise our analysis. What sort of statistical analysis is

![image-20211203122244918](../images/image-20211203122244918.png)

## Text Clustering

**Clustering** is an **unsupervised learning** process through which objects are classified into groups called clusters. In supervised learning, that is, text classification/categorization, we are provided with a collection of preclassified training examples, and the task of the machine is to learn the descriptions of classes in order to be able to classify a new unlabeled document. In the case of clustering, the problem is t**o group the given unlabeled documents into meaningful clusters without any prior information**. Any labels associated with objects are obtained solely from the data. One application of clustering is the analysis and navigation of big text collections such as Web pages. 

![image-20211203115624838](../images/image-20211203115624838.png)

The basic **assumption**, called the **cluster hypothesis**, states that **relevant documents tend to be more similar to each other** than to nonrelevant ones. If this assumption holds for a particular document collection, the clustering of documents based on the similarity of their content may help to improve the search effectiveness.

![image-20211203115645321](../images/image-20211203115645321.png)

A clustering task may include the following components:

- Problem representation, including feature extraction, selection, or both,
- Definition of proximity measure suitable to the domain,
- Clustering of objects, and
- Evaluation

<img src="../images/image-20211203120945525.png" alt="image-20211203120945525" style="zoom: 67%;" />

## Flat Clustering *VS.* Hierarchical Clustering 

A **flat** (or partitional) clustering produces a single partition of a set of objects into disjoint groups, whereas a hierarchical clustering results in a nested series of partitions.

| ![Hello, Carrot2! - Carrot2 docs](../images/carrot2-search-app-light.jpg) | <img src="../images/image-20211203120002808.png" alt="image-20211203120002808" style="zoom:50%;" /> |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
|                    ***Flat Clustering***                     |                ***Hierarchical Clustering***                 |

## Soft Clustering *VS.* Hard Clustering 

**Hard clustering** computes a hard assignment – **each document is a member of exactly one cluster**. The assignment of **soft clustering** algorithms is soft – **a document’s assignment is a distribution over all clusters**. In a soft assignment, a document has fractional membership in several clusters.Latent semantic indexing, a form of dimensionality reduction, is a soft clustering algorithm.

![image-20211203135002270](../images/image-20211203135002270.png)  

## K-Means Clustering

**K-means** is the most important **flat clustering** algorithm. Its objective is to **minimize the average squared Euclidean distance** of documents from their **cluster centers** where a cluster center is defined as the mean or centroid of the documents in a cluster. T k-means clustering process is as follows:

- Select as initial cluster centers K randomly selected documents, the **seeds**.
- The algorithm then moves the cluster centers around in space in order to minimize RSS. This is done iteratively by repeating two steps until a stopping criterion is met:
  - Reassigning documents to the cluster with the closest centroid.
  - Recomputing each centroid based on the current members of its cluster.



![image-20211203120150625](../images/image-20211203120150625.png)

The process terminates when RSS falls below a threshold and a fixed number of iterations has been completed.

## Graph-Based Clustering

Previous clustering techniques rely upon linguistics, whereas you may want to consider conceptual relationships between terms - e.g. *a fuselage is [Part_of] an airplane* or *an airplane [Transports] passengers* - based on ontological ressources.

![image-20211203135441349](../images/image-20211203135441349.png)

Thus, you may consider **graph-based clustering** techniques that attempts to detect **densely connected** groups of **vertices**, with only sparser connections between groups

| ![image-20211203135313626](../images/image-20211203135313626.png) | ![image-20211203135204863](../images/image-20211203135204863.png) | ![image-20211203135204863](../images/image-20211203135204863.png) |
| :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| **Well-know patterns**<br />*(Cliques, plexes, cores, components)* | **Partitioning** <br />*(Nb and size of communities known)*  | **Community detection**<br />*(Nb and size of communities unknown)* |

## Text Classification

**Text classification** is a special instance of the machine learning-based classification problem, where the **input** data point(s) is **document** and the goal is to **categorize the document into one or more buckets** (called a **class** or **category**) from a set of predefined buckets (classes). The “text” can be of arbitrary length: a character, a word, a sentence, a paragraph, or a full document.

![image-20211129162936464](../images/image-20211129162936464.png)

In theory, a machine learns how to classify by **estimating an unknown function *f*** such as ***y = f(x) + ∈*** or ***y = f(x<sub>1</sub>,x<sub>2</sub>,...,x<sub>n</sub>) + ∈***

In practice, the problem is to **estimate the unknown function *f* linking *X* and *y***, that is, to find a fuction:

<img src="../images/image-20211130155027345.png" alt="image-20211130155027345" style="zoom: 67%;" />





## End-to-End Exercise

Contracts (legal contracts, product specification, collaboration agreement, etc.) often refer to external prescriptive documents containing constraints (legal, certification, customers, etc.). Thus, we want to identify sentences that contain cross-references to other documents so as to, *in fine*, create a graph of documents linked by cross references. To identify cross-references, we could create regular expresssions, but we cannot enumerate all possible wordings. Therefore, we will use statistical/machine learning techniques to train the machine to automatically classify each sentence as legal sentence (L) or not legal sentence (NL).

<img src="../images/image-20211203150354426.png" alt="image-20211203150354426" style="zoom:80%;" />

Given this [dataset](https://gricad-gitlab.univ-grenoble-alpes.fr/pinquier/nlp_tutorials/-/blob/main/code/Datasets/Exercice_Classification/dataset.csv) in CSV Format, you shall:

- Load the data in a dictionary
- Clean the data (if needed!)

- Shuffle the dataset

```python
import random

def shuffle_dictionary(dic):

    l = list(dic.items())
    random.shuffle(l)
    dic = dict(l)
    return dic
```

- Split the dataset into a training set (all sentences except the last 20) and a testing set (the last 20 sentences)

- Transform the dataset into Boolean features vectors based on the presence or not of...

  - ```python
    prescriptive_terms = ["specified", "as specified in", "as specified at", "as specified under", "as detailed in", "detailed in", "as defined in", "defined in", "in accordance with", "in accordance to","prescribed in," "as set out in", "specified herein", "comply with", "in agreement with", "i.a.w", "be compatible with", "conform to", "stated in", "refer to"]
    ```
    
  - Structure of document sections - e.g.,  $, section, sec, chapter, chap, page, p
  - Term as a blend of capitalised characters and digits - e.g., ISO9000, CS25
  - Term with multiple capitalized letters  - e.g., ISO, CS
  - Initials of standards organisation - e.g., https://en.wikipedia.org/wiki/Standards_organization
  - Legal document - e.g., standard, regulation, law, norm, rule, code, procedure
  - Prepositions - e.g., as, in, with, to
  - Term with multiple dots - e.g., 2.10.3.11
  - Term with multiple occurences of the dash - e.g., ECSS-E-40
  - Brackets - e.g., [, (, {, }, ), ]

You will have to make an extensive use of regular expressions (regex) that you can test [here](https://regex101.com/r/l7c8iF/1/). For instance use the regular expression *([A-Za-z]+[\d@]+[\w@]*|[\d@]+[A-Za-z]+[\w@]*)* with the test string *This is a test ISO9000 with a rule*. Use the *re* Python package (import re). For example,

```python
import re

# Check if the sentence contains a term that is a blend of capitalised characters and digits
def contains_mix_of_uppercases_digits(sentence):
    pattern = "([A-Za-z]+[\d@]+[\w@]*|[\d@]+[A-Za-z]+[\w@]*)"
    return re.search(pattern, sentence)
```

- Write the features vectors and corresponding class (aka. category, label) in a txt file

```
0 1 1 0 1 1 0 0 1,L
1 1 1 0 1 0 0 0 1,NL
1 1 1 0 1 0 1 1 1,NL
0 1 1 0 1 1 1 0 1,L
1 1 0 0 1 1 0 0 1,L
1 1 0 1 1 0 0 0 1,NL
```

- Train a machine learning algoritm ([Support Vector Machine algorithm](https://scikit-learn.org/stable/modules/svm.html) as it usually outperforms competing alternatives for text) from the [Scikit Learn](https://scikit-learn.org/stable/index.html) library with the training set before testing it on the testing set:

```python
# Train a machine learning algorithm and classify the testing set
def classify(features_training, labels_training, features_testing):

    # Select SVM as machine learning algorithm
    clf = svm.SVC()
    # Train an SVM machine learning algorithm
    clf.fit(features_training, labels_training)
    # Apply the classified on the testing set
    testing_labels = clf.predict(features_testing)
    # Return the predicted labels
    return testing_labels
```

- Print the sentence to classify, the predicted category, and the actual category, e.g.:
  - The SCC Vehicle shall be compatible with the Ground thermal environment defined in the Thermal Environment and Test Requirements Specification [NR 05006] § 5.1. predicted: L actual: L
  - The product shall ensure that only company employees or external users with company-approved user IDs may have product access. predicted: NL actual: NL
  - Design Verification of the equipment shall be established by performing the Qualification inspections and tests detailed in Section 4.2.3.2 on a minimum of one unit. predicted: L actual: NL
  - The system shall allow a real estate agent to query MLS information predicted: NL actual: NL
  - The following tests shall be carried out in the sequence specified in Section 4.2.3.2. predicted: L actual: NL
  - The product shall help System Administrators to analyze the transactions with ad-hoc reporting and KPIs. predicted: NL actual: NL
  - For each download of ECV Data Product/s the System shall log the visitor data. predicted: NL actual: L
  - Reports must be generated within one button click and exported within one button click (after selecting the report). predicted: NL actual: L
  - The estimator shall search for available recycled parts using a list of preferred parts suppliers. predicted: NL actual: NL
  - The product shall generate a CMA report in an acceptable time. predicted: NL actual: NL
  - The leads washing functionality will store any potential lead duplicates returned by the enterprise system. predicted: NL actual: NL
  - Upon completion of the Resampling processing step, the System shall store Resampled Data for reuse in future processing. predicted: NL actual: NL
  - Cookies containing WCS login information about a user will not be stored on a user’s computer. predicted: NL actual: NL
  - User access should be limited to the permissions granted to their role(s) Each level in the PCG hierarchy will be assigned a role and users will be assigned to these roles. predicted: NL actual: L
  - The estimator shall not apply recycled parts to the collision estimate if no available parts are returned. predicted: NL actual: NL
  - The system shall allow a Program Administrator or Nursing staff member to remove a student from a clinical lab section. predicted: NL actual: NL
  - Callers and supervisors must be able to accomplish any system task within 2 minutes. predicted: NL actual: L
  - For every produced ECV Data Product, the PUG shall include the description of retrieval errors. predicted: NL actual: NL
  - The System Team shall strengthen their professional excellence by cooperating with the Professional business and sales managers. predicted: NL actual: NL
  - A portfolio consists of documentation that would provide proof of a purchase such as the documentation that is received from a car rental agency that is more than a sales receipt. predicted: NL actual: NL
